package sachtech.jainagdev.hdwallpaper.rxjavademo.operators

import android.util.Log
import io.reactivex.Observable
import io.reactivex.functions.Function

class ConcatMapOperator {
fun performConcatMapOperation(){
    var concatMapObservable=Observable.just("jsks","jhd","sjsj","djdj")
    concatMapObservable.concatMap(object:Function<String,Observable<String>>{
        override fun apply(t: String): Observable<String> {
            return Observable.just(t).map { t.toUpperCase() }
        }

    }).subscribe({it-> Log.d("ConcatMap",it)})
}
}