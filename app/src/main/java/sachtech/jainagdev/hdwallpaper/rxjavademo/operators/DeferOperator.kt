package sachtech.jainagdev.hdwallpaper.rxjavademo.operators

import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.Callable

class DeferOperator {

    fun performDeferOperation() {
        var dataList = arrayListOf<String>("dhgd", "dshgdh", "sdgds")
        var deferObservable = Observable.defer(object : Callable<Observable<ArrayList<String>>> {
            override fun call(): Observable<ArrayList<String>> {
                return Observable.just(dataList)
            }

        })
        deferObservable.subscribeOn(AndroidSchedulers.mainThread())
                .subscribe { it-> for(i in 0..(it.size-1)){
                    Log.d("Defer Operator",it[i])
                }
                }
    }
}