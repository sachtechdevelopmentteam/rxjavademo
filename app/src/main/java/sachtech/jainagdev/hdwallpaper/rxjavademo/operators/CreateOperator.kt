package sachtech.jainagdev.hdwallpaper.rxjavademo.operators

import android.util.Log
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers

class CreateOperator {
    fun performCreateOperation(){
        var dataList= arrayListOf<String>("dhgd","dshgdh","sdgds")
        var createObservable=Observable
                .create(object :ObservableOnSubscribe<String>{
                    override fun subscribe(emitter: ObservableEmitter<String>) {
                        for (i in 0..(dataList.size-1)){
                            emitter.onNext(dataList[i])
                        }
                    }

                }).subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({it-> Log.d("Create Operator",it)})
    }
}