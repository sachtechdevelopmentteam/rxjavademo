package sachtech.jainagdev.hdwallpaper.rxjavademo.subject

import android.util.Log
import io.reactivex.subjects.ReplaySubject

class ReplaySubject {
    var Tag=ReplaySubject().javaClass.simpleName
    fun getReplay(){
        var replaySubject=ReplaySubject.create<Any>()
        replaySubject.onNext("dshgd")
        replaySubject.subscribe({it -> Log.e(Tag,"Observer 1 onNext:$it")},
                {error->Log.e(Tag,error.localizedMessage)},
                {Log.e(Tag,"Observer 1 onCompleted")},
                {onSubscribe-> Log.e(Tag,"Observer 1 onSubscribe")})
        replaySubject.onNext(1)
        replaySubject.onNext("sdfghssd")
        replaySubject.subscribe({it -> Log.e(Tag,"Observer 2 onNext:$it")},
                {error->Log.e(Tag,error.localizedMessage)},
                {Log.e(Tag,"Observer 2 onCompleted")},
                {onSubscribe-> Log.e(Tag,"Observer 2 onSubscribe")})
        replaySubject.onNext("jhsdfsd")
        replaySubject.subscribe({it -> Log.e(Tag,"Observer 3 onNext:$it")},
                {error->Log.e(Tag,error.localizedMessage)},
                {Log.e(Tag,"Observer 3 onCompleted")},
                {onSubscribe-> Log.e(Tag,"Observer 3 onSubscribe")})
        replaySubject.onComplete()
    }
}