package sachtech.jainagdev.hdwallpaper.rxjavademo.operators

import android.util.Log
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class BufferOperator {



    fun performBufferOperation(){
        var bufferObservable=Observable.just("simran","hsd","sah","sks","sah")
       // var bufferObserver=getBufferObserver()
        bufferObservable.buffer(2).
                subscribeOn(AndroidSchedulers.mainThread()).subscribe({it->Log.e("Buffer Operator"," $it")},
                {error -> Log.e("Buffer Operator",error.localizedMessage)},{},{})
    }


}