package sachtech.jainagdev.hdwallpaper.rxjavademo.operators

import android.util.Log
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers

class SwitchMapOperator {

    fun performSwitchMapOperation(){
        var switchMapObservable=Observable.just("dghf","asjh","wshg","shs")
        switchMapObservable.switchMap(object :Function<String,ObservableSource<String>>{
            override fun apply(t: String): ObservableSource<String> {
                return Observable.just(t)
            }

        }).subscribeOn(Schedulers.io()).
                subscribe({it->Log.d("SwitchMap",it)},{},
                {Log.d("SwitchMap","onCompleted")},{})
    }
}