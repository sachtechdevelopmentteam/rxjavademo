package sachtech.jainagdev.hdwallpaper.rxjavademo.operators

import android.util.Log
import io.reactivex.Observable
import io.reactivex.functions.Function

class FlatMapOperator {
    fun performFlatMapOperation(){
        var flatMapObservable= Observable.just("hdf","dhd","ghdf","fhdf","dhdfj","shsd")
    flatMapObservable.flatMap(object:Function<String,Observable<String>>{
        override fun apply(t: String): Observable<String> {
            return Observable.just(t).map{t.toUpperCase()}
        }

    }).subscribe{it-> Log.d("FlatMap",it)}

    }
}