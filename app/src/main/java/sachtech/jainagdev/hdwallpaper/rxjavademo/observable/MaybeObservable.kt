package sachtech.jainagdev.hdwallpaper.rxjavademo.observable

import io.reactivex.Maybe
import io.reactivex.MaybeEmitter
import io.reactivex.MaybeObserver
import io.reactivex.MaybeOnSubscribe
import io.reactivex.disposables.Disposable
import io.reactivex.internal.operators.maybe.MaybeJust

class MaybeObservable {
    fun getMayBeObservable():Maybe<String>{
        return Maybe.create(object :MaybeOnSubscribe<String>{
            override fun subscribe(emitter: MaybeEmitter<String>) {

            }

        })
    }

    fun getMaybeObserver():MaybeObserver<String>{
        return object :MaybeObserver<String>{
            override fun onSuccess(t: String) {

            }

            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {

            }

            override fun onError(e: Throwable) {

            }

        }
    }
}