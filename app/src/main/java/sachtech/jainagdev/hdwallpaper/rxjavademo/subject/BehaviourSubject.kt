package sachtech.jainagdev.hdwallpaper.rxjavademo.subject

import android.util.Log
import io.reactivex.subjects.BehaviorSubject

class BehaviourSubject {
    var Tag=BehaviourSubject().javaClass.simpleName
    fun getBehaviour(){
        var bSubject=BehaviorSubject.create<Any>()
        bSubject.onNext(1)
        bSubject.subscribe({it->Log.e(Tag,"Observer 1 onNext: $it")},
                { error->Log.e(Tag,error.localizedMessage)},
                {Log.e(Tag,"Observer 1 onCompleted")},
                {onSubscribe-> Log.e(Tag,"Observer 1 onSubscribe")})
        bSubject.onNext(2)
        bSubject.onNext(3)
        bSubject.subscribe({it -> Log.e(Tag,"Observer 2 onNext :$it")},
                {error-> Log.e(Tag,error.localizedMessage)},
                {Log.e(Tag,"Observer 2 onCompleted")},
                {onSubscribe-> Log.e(Tag,"Observer 2 onSubscribe")})
        bSubject.onNext(4)
        bSubject.subscribe({it -> Log.e(Tag,"Observer 3 onNext :$it")},
                {error-> Log.e(Tag,error.localizedMessage)},
                {Log.e(Tag,"Observer 3 onCompleted")},
                {onSubscribe-> Log.e(Tag,"Observer 3 onSubscribe")})
        bSubject.onComplete()
    }
}