package sachtech.jainagdev.hdwallpaper.rxjavademo.observableCombination

import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import sachtech.jainagdev.hdwallpaper.rxjavademo.ZipObject
import java.util.function.BiFunction

class Zip {
    fun performZipCombination(){
        var observable1= Observable.just("abcd","adgsg","sdh","sja")
        var observable2=Observable.just(1,2,3,4,5)
        var zipObservable=Observable.zip(observable1,observable2,object :io.reactivex.functions.BiFunction<String,Int,ZipObject>{
            override fun apply(t1: String, t2: Int): ZipObject {
                return ZipObject(t2,t1)
            }

        })
        zipObservable.subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it->Log.d("Zip","number:${it.number} alphabets: ${it.alphabets}")
                })
    }
}