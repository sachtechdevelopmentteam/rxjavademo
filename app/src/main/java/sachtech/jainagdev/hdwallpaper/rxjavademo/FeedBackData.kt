package sachtech.jainagdev.hdwallpaper.rxjavademo

import com.google.gson.annotations.SerializedName


/**
 * Created by Rahul Sharma on 25/01/19 .
 */
data class FeedBackData(
    @SerializedName("Message")
    val message: String?,
    @SerializedName("Status")
    val status: Boolean?
)