package sachtech.jainagdev.hdwallpaper.rxjavademo.subject

import android.util.Log
import io.reactivex.subjects.AsyncSubject

class AsyncSubject {
    var Tag=AsyncSubject().javaClass.simpleName
    fun getAsyncSubject(){
        var asynSubject=AsyncSubject.create<Any>()
        asynSubject.onNext("hjsdh")
        asynSubject.onNext(1)
        asynSubject.subscribe({it->Log.e(Tag,"Observer 1 onNext: $it")},
                {error->Log.e(Tag,error.localizedMessage)},
                {Log.e(Tag,"Observer 1 onCompleted")},
                {Log.e(Tag,"Observer 1 onSubscribe")})
        asynSubject.onNext(33)
        asynSubject.subscribe({it-> Log.e(Tag,"Observer 2 onNext: $it")},
                {error-> Log.e(Tag,error.localizedMessage)},
                {Log.e(Tag,"Observer 2 onCompleted")},
                {Log.e(Tag,"Observer 2 onSubscribe")})
        asynSubject.onNext("hdfsdh")
asynSubject.onComplete()
    }
}