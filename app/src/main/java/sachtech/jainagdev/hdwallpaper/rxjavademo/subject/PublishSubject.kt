package sachtech.jainagdev.hdwallpaper.rxjavademo.subject

import android.util.Log
import io.reactivex.subjects.PublishSubject
import java.lang.Error

class PublishSubject {
var Tag="Publish Subject"
    var pSubject = PublishSubject.create<Any>()
    fun ab(){
        pSubject.onNext("hfjhf")
        pSubject.subscribe({ it -> Log.e(Tag,"Observer 1 onNext: $it") },
                {  Throwable -> Log.e(Tag,Throwable.localizedMessage) },
                { Log.e(Tag,"Observer 1 onCompleted") },
                { on1 -> Log.e(Tag,"Observer 1 onSubscribe") })

       pSubject.onNext("URYHRUFG")
        pSubject.onNext("gfgfgbhf")
        pSubject.subscribe({it -> Log.e(Tag,"Observer 2 onNext: $it")},
                { error->Log.e(Tag,error.localizedMessage) },
                {Log.e(Tag,"Observer 2 onCompleted")},
                {onSubscribe-> Log.e(Tag,"Observer 2 onSubscribe")})

        pSubject.onNext("djjsd")
        pSubject.onNext("fdkjkl")

        pSubject.subscribe({it -> Log.e(Tag,"Observer 3 onNext: $it")},
                { error->Log.e(Tag,error.localizedMessage) },
                {Log.e(Tag,"Observer 3 onCompleted")},
                {onSubscribe-> Log.e(Tag,"Observer 3 onSubscribe")})

    pSubject.onNext("shghsd")
        pSubject.onComplete()
    }
    }


