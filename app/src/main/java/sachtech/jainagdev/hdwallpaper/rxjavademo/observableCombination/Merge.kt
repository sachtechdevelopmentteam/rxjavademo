package sachtech.jainagdev.hdwallpaper.rxjavademo.observableCombination

import android.util.Log
import io.reactivex.Observable

class Merge {
    fun performMergeCombination(){
        var observable1= Observable.just("abc","def","ghi","jkl")
        var observable2=Observable.just(1,2,3,4,5)
        var mergeObservable=Observable.merge(observable1,observable2).subscribe {
            it->
            Log.d("Merge","output:$it")
        }
    }
}