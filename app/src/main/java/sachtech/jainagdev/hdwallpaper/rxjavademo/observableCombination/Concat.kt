package sachtech.jainagdev.hdwallpaper.rxjavademo.observableCombination

import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class Concat {
    fun performConcatCombination(){
        var observable1= Observable.just(1,2,3,4,5)
        var observable2=Observable.just(6,7,8,9,10)
    var concatObservable=Observable.concat(observable1,observable2)
        concatObservable.subscribeOn(AndroidSchedulers.mainThread())
                .subscribe { it->
                    Log.d("Concat","output: $it")
                }
    }
}