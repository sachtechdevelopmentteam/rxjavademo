package sachtech.jainagdev.hdwallpaper.rxjavademo.operators

import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class MapOperator {
    fun performMapOperation(){
var mapObservable= Observable.just("simran","dgh","hsdjk")
        mapObservable.map{
            it-> it+"xyz"
        }.subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({it-> Log.d("Map Operator",it)})
    }
}