package sachtech.jainagdev.hdwallpaper.rxjavademo.operators

import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class FilterOperator {

    fun performFilterOperation(){
        var performObservable=Observable.just("apple","simran","shivali","abcd","sjfh")
performObservable.filter {
    it-> it.startsWith("s",false)
}.subscribeOn(AndroidSchedulers.mainThread())
        .subscribe({it->Log.d("Filter Operator",it)})
    }
}