package sachtech.jainagdev.hdwallpaper.rxjavademo.observable

import android.util.Log
import io.reactivex.Completable
import io.reactivex.CompletableEmitter
import io.reactivex.CompletableOnSubscribe

class CompletableObservable {

    fun getCompleteObservable():Completable{
return Completable.create(object:CompletableOnSubscribe{
    override fun subscribe(emitter: CompletableEmitter) {
      Thread.sleep(1000)
        emitter.onComplete()
    }

})
    }

    init {
        var completeObservable=getCompleteObservable()
        completeObservable.subscribe({Log.d("Completable","Status Completed")})
    }
}