package sachtech.jainagdev.hdwallpaper.rxjavademo.network

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import sachtech.jainagdev.hdwallpaper.rxjavademo.FeedBackData
import sachtech.jainagdev.hdwallpaper.rxjavademo.observable.SingleObservable

interface ApiInterface {
    companion object {
        val KEY: String = "boogie@123_*"
    }

    @FormUrlEncoded
    @POST("Feedback/feedbackSave")
    fun sendFeedbak(
            @Field("API_KEY") API_KEY: String = KEY,
            @Field("user_id") user_id: String,
            @Field("feedback") feedback: String
    ): Single<FeedBackData>
}